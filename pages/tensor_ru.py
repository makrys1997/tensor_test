from pages.base import BasePage
from selenium.webdriver.common.by import By

power_in_people_block_selector = (By.CSS_SELECTOR, 'div[class*="tensor_ru-Index__block4-content"]')
about_button_selector = (By.CSS_SELECTOR, 'a[href="/about"]')
working_block_selector = (By.CSS_SELECTOR, 'div[class*="tensor_ru-section tensor_ru-About__block3"]')
images_list_selector = (By.CSS_SELECTOR, 'img[class*="tensor_ru-About__block3-image"]')


class TensorruPage(BasePage):
    def __init__(self, browser):
        super().__init__(browser)

    def open(self):
        self.browser.get('https://tensor.ru')

    def power_in_people_block(self):
        return self.find(power_in_people_block_selector)

    def about_button(self):
        return self.find(about_button_selector)

    def working_block(self):
        return self.find(working_block_selector)

    def images_list(self):
        return self.find_all(images_list_selector)
