class BasePage:
    def __init__(self, browser):
        self.browser = browser

    def url(self):
        return self.browser.current_url

    def get_title(self):
        return self.browser.title

    def find(self, args):
        return self.browser.find_element(*args)

    def find_all(self, args):
        return self.browser.find_elements(*args)

    def click_with_script(self, element):
        self.browser.execute_script("arguments[0].click();", element)

    def scroll_down(self):
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
