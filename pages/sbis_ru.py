from pages.base import BasePage
from selenium.webdriver.common.by import By


contact_button_selector = (By.CSS_SELECTOR, 'li[class*="sbisru-Header__menu-item-1"] a')
tensor_banner_selector = (By.CSS_SELECTOR, 'div[class*="sbisru-Contacts__border-left"] a')
region_chooser_selector = (By.CSS_SELECTOR, 'span[class*="sbis_ru-Region-Chooser__text"]')
partners_list_selector = (By.CSS_SELECTOR, 'div[class="sbisru-Contacts-List__col ws-flex-shrink-1 ws-flex-grow-1"]')
kamchatka_link_selector = (By.CSS_SELECTOR, 'span[title="Камчатский край"]')
city_block_id = (By.ID, 'city-id-2')


class SbisruPage(BasePage):
    def __init__(self, browser):
        super().__init__(browser)

    def open(self):
        self.browser.get('https://sbis.ru')

    def contact_button(self):
        return self.find(contact_button_selector)

    def tensor_banner(self):
        return self.find(tensor_banner_selector)

    def region_chooser(self):
        return self.find(region_chooser_selector)

    def partners_list(self):
        return self.find(partners_list_selector)

    def kamchatka_link(self):
        return self.find(kamchatka_link_selector)

    def city_block(self):
        return self.find(city_block_id)
