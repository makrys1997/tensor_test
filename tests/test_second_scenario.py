import time

from pages.sbis_ru import SbisruPage


def test_2(browser):
    """1) Перейти на https://sbis.ru/ в раздел <Контакты>"""
    current_page = SbisruPage(browser)
    current_page.open()
    current_page.contact_button().click()
    assert 'https://sbis.ru/contacts' in current_page.url()

    """2) Проверить, что определился ваш регион (в моём случае Новосибирская обл.) и есть список партнеров."""
    assert current_page.region_chooser().text == 'Новосибирская обл.'
    assert current_page.partners_list()

    """3) Изменить регион на Камчатский край"""
    current_page.region_chooser().click()
    current_page.kamchatka_link().click()
    time.sleep(1)  # Понимаю, что грубо, но правильный вариант задержки реализовать не получилось

    assert current_page.region_chooser().text == 'Камчатский край'  # Проверяем регион
    assert current_page.city_block().text == 'Петропавловск-Камчатский'  # Проверяем блок партнеры
    assert '41-kamchatskij-kraj' in current_page.url()  # Проверяем URL
    assert 'Камчатский край' in current_page.get_title()  # Проверяем название вкладки браузера
