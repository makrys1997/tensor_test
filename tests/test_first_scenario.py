from pages.sbis_ru import SbisruPage
from pages.tensor_ru import TensorruPage


def test_1(browser):
    """1) Перейти на https://sbis.ru/ в раздел <Контакты>"""
    current_page = SbisruPage(browser)
    current_page.open()
    current_page.contact_button().click()
    assert 'https://sbis.ru/contacts' in current_page.url()

    """2) Найти баннер Тензор, кликнуть по нему"""
    assert current_page.tensor_banner()
    current_page.tensor_banner().click()

    """3) Перейти на https://tensor.ru/"""
    current_page = TensorruPage(browser)
    current_page.open()

    """4) Проверить, что есть блок "Сила в людях"""
    assert current_page.power_in_people_block()

    """5) Перейдите в этом блоке в "Подробнее" и убедитесь, что открывается https://tensor.ru/about"""
    assert current_page.about_button()
    current_page.click_with_script(current_page.about_button())
    assert current_page.url() == 'https://tensor.ru/about'

    """6) Находим раздел <Работаем> и проверяем, что у всех фотографий 
    хронологии одинаковые высота (height) и ширина (width)"""
    assert current_page.working_block()

    images_list = current_page.images_list()

    width = []
    height = []

    for image in images_list:
        width.append(image.get_attribute('width'))
        height.append(image.get_attribute('height'))

    assert len(set(width)) == 1  # Проверяем, что все 4 значения ширины одинаковы
    assert len(set(height)) == 1  # Проверяем, что все 4 значения высоты одинаковы
